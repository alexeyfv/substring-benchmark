using System.Text.RegularExpressions;
using BenchmarkDotNet.Attributes;

namespace Benchmarks;

[MemoryDiagnoser]
public class Benchmark
{
    private readonly string[] _data = new[]
    {
        "L4pKzQ5eZJ:r3Id6FvC2gPXMnQoqRHWmlCJ6NSwmpEsD8tiF",
        "WgMJyRkFr14twUkgsEJKyO0uFbhPhgs6Y1YtH:inFttm7m8",
        "Pw0P9pKzMpMOvKmT0d:A4XuxF8UzEGh4i0s7YZ2CZ6cW",
        "0lBXM7Eic:kqzIaHYgCoNq9ru6bzRFi9bOlRGwIbQmBbsSQI9",
        "2Bq8w5wI2:iG8bJExaCmxDiIVi6FR7ZpPy8ZtEGNTR88PMiN",
        "3YbnGZKIt:gIgXXhMTLsbjw3D6LzF7H4c4UGb7T5yw2JtGOg9",
        "RJz:tcXzmYZVmTJRVqqYfeAFx9eRgUAp5sKSTbFJ0B",
        "m2ZcMZ2tD3E8aZzGf:OvIXEe9Dl6KEmwJ3NtC9Rtm9",
        "8zyY83tGdCtQV:lI8X5fWMGvCpK6BbiE6HcTVPl",
        "A1ftFVn5YjgBjG:q9cVo2ei3c8ukGOFCZgSSXf8",
        "sRkt3mC1Bl6h:YmyPDSNgwTtKoEazCIZ44JW2",
        "pO3vNs7CN5zk:iD9KmcC2g2DqQF8y7At8bY5po",
        "XjSY9XQTub1x:vmT0sliRfzlvPIixElx8hpo5",
        "RYXbUlFhWTaU:HvWJiC8fDzjPvjVNRHKv01Xm",
        "y6ZJLxN4t7VO:Vb7DX9eaQXHhOZcUgKuCSxat",
        "kNqrJogUL:mEIE95Tf6ViGop2uxOBaHvRt",
        "dOcS6tnCJJE:c6l0IJc2pPM4f6wiFkVWmBgn",
        "NWqr1S5bUx58:dQvLGTpPmH7gPmzNOLvCctqR",
        "7rEElR0NiL:bVRe3zqsHdC1xbtuWtWMCnQi",
        "tEqVRhF:nWvCdZ4cZXHOCmGmbmcXStjI",
        "X1n2Mlqkc59uV:a5By3k4un61SxCKpdaD4FHIm",
        "GY5R4cqyIa:mbLPbw9y6Q69NSWzm3rEhDmc",
        "FZg4NbXV8G0j:QhchvlMQeIqsxlHUVSRKdj4b",
        "bTGjaZ5:vWsN6BjDXumYNWFnXhJGQ6Vq",
        "YLOFGZP3Lu:qA9cW8qavNOpEL1Oa6KvYyUV",
        "3Nj5E8XgD:vpXPSKpQsRKuCIMtxoAe9QWP",
        "3IYVmB7WcO2m:j3nlFWxYenIn1oqC89Kdw9do",
        "C5LWuf5HzmM:PU4cB2Bqz7nANPyysmqnLDiA",
        "kCfqrPmmFTX:Rw5k5b56Xr9GfY95e41SmRNB",
        "rNVK5FX5txzG:H5XIN99QyYNBq2TuSRJ52wUo",
        "NXo3VULFyFlY:bV0Dm1DLTPaK0oZ8F7YgGBfW",
        "LmNV1fSBnQ2w:OVtKxD3VlZ2MjTfD5OHcM2ct",
        "YZNgx0qu3g:RwID7X3Uqglr0X2kQ0p7b0Qo",
        "A7tW8eWkA:iW6x0cPeSh4QqkZl9D0Mn5qc",
        "5i2o4LdJrk:hG4PQK0M32lmVoJGXbHJQXW",
        "XVLczMrKB0y:UM4BpZnlJt9tnF5Skp5JXxq8",
        "B9Xp2raA1:O5lOnGvbF8yiL8nyQiBoDZXC",
        "EN4FgZj6TI:mbcmEwS5MjLWIu1q7KnA2Cxn",
        "kSgbZIZeFQ6:adTw2mFZPXjT9yA3iLmb1XiL",
        "I2XbwnPX0:nQOM9Kfrtkq6eWn6DYIEHJKW",
        "0lD2SXe2p7N:GuF7D2T2RczP1SliFVcUDXal",
        "o6HgXB0IUM:h0zLNLWg5OBEmSEIP0G57aOc",
        "Aoc9pShEiLv:OjY6pMd6nLlvxKQTfIdB0Zv8",
        "YSPgV2q6B6n:X0V4xNl3QbzJgX4M3ZIKKj2V",
        "q5gEzmYbLm:vFvLbHcx1m7tXylMk2X3RiWw",
        "PMzRnMszN9C:piXcL8sQZMnMBef3lYlUJsiw",
        "ZvYRbFn8bm6:0aMn6TSB6XTBvI61YMQwbqLu",
        "Fk8aTgy6Ib:qzXKo0y4iJwxDfjzOk2ENpOZ",
        "qIqsUf7J0W:kGIhnIa0Z5CG9hHjYMVJKh1o",
        "qXeCb5mM0hA:HSkgrmuT1aL9EKf4VB9SyZ0D",
    };

    private readonly char _symbol = ':';

    private readonly string _pattern = ":(.*)$";

    [Benchmark]
    public void Substring()
    {
        var result = new List<string>();

        foreach (var s in _data)
        {
            var i = s.IndexOf(_symbol);
            result.Add(s.Substring(i));
        }
    }

    [Benchmark]
    public void RangeOperator()
    {
        var result = new List<string>();

        foreach (var s in _data)
        {
            var i = s.IndexOf(_symbol);
            result.Add(s[i..]);
        }
    }

    [Benchmark]
    public void SkipWhile()
    {
        var result = new List<string>();

        foreach (var s in _data)
        {
            result.Add(new string(s.SkipWhile(x => x != _symbol).ToArray()));
        }
    }

    [Benchmark]
    public void Split()
    {
        var result = new List<string>();

        foreach (var s in _data)
        {
            result.Add(s.Split(':')[1]);
        }
    }

    [Benchmark]
    public void RegularExpressions()
    {
        var result = new List<string>();

        foreach (var s in _data)
        {
            result.Add(Regex.Match(s, _pattern).Groups[1].Value);
        }
    }

    [Benchmark]
    public void Span()
    {
        var result = new List<string>();

        foreach (var s in _data)
        {
            var i = s.IndexOf(_symbol);
            var span = s.AsSpan(i);
            result.Add(new string(span));
        }
    }
}